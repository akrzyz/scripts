### How do I get set up? ###
Just clone it and add to $PATH

### Content ###
* **f** find wrapper *f $FILE [$DIR]*
* **g** wrapper for *$EDITOR*. Change *$FILE[:+#]$LINE_NUMBER* to *$FILE +$LINE_NUMBER* and look for file in *$PWD* if file is not a full path.
* **gInS** - Grep In Source, grep -rn wrapper. *gInS [$grep_options] $pattern [$directory]*. Special options are *t* grep only in test files, *a* grep in source and test files, without *a* or *t* grep only in source files.
* **fr** find and replace *$PATERN* for *$REPLACMENT* in *$PWD*, *fr $PATERN $REPLACMENT*
* **tagsGen** generate ctags for repository in *$projectroot/.tags/*. To generate all tags for project run *tagsGen all*. For more information run *tagsGen -h*. For easy way to use generated tags files check [vim-cp-tags](https://bitbucket.org/akrzyz/vim-cp-tags).
* **eutraCellId** - merge or split eutra_cell_id. *eutraCellId $eutra_cell_id* split for bts_id and lcr_id or *eitraCellId $bts_id $lcr_id* merge in eutra_cell_id.
* **knife** prepare knife. *knife -b $target* builds knife binary and prepares txz file, *knife -k* prepares kinfe.zip file using all changed files.
* **runUt** - *runUt $TARGET* build and run unit test *$TARGET*, supports many options like sanitizer, run with gdb, enable logs, test filter, target pattern matching... For more informations run *runUt -h*

### More details ###
For more details run each script with -h option

### Dependencies ###
**tagsGen** need ctags with ttcn language support, orginal ctags does not have support for ttcn.
[universal ctags](https://github.com/universal-ctags/ctags) provides valid ctags implementation.

### How to run ut? ###
./ut/runUt.sh

### Contribution guidelines ###
fill free to make pull requests