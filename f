#!/bin/bash
function displayHelp()
{
    echo "f usage:
f patern             find .   -iname *patern*
f patern dir         find dir -iname *patern*
f -flags patern dir  find dir -flags *patern*

no option: '-v .rej|\.tmp|\.swp|\.svn|\.orig'
"
}

test "$1" == "-h" || test "$1" == "--help" && displayHelp && exit

OMITED='\.rej|\.tmp|\.swp|\.svn|.\orig'

if [ $# -eq 1 ]; then
    FLAGS="-iname"
    PATERN="$1"
    WHERE="."
elif [ $# -eq 2 ]; then
    FLAGS="-iname"
    PATERN="$1"
    WHERE=$2
elif [ $# -eq 3 ]; then
    FLAGS="$1"
    PATERN="$2"
    WHERE="$3"
else
    echo "too many parameters"
fi

find $WHERE $FLAGS "*$PATERN*" | egrep -v $OMITED | grep -i $PATERN --color=auto

