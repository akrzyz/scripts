#!/bin/bash
PASS=0
FAIL=1
GLOBAL_RESULT=$PASS
GLOBAL_SUMMARY=()

function Sort()
{
    echo `for i in $@; do echo $i; done | sort`
}

function assert()
{
    TEST_NAME=${FUNCNAME[1]}
    EXPECTED=`echo "$1" | tr "\n" " "`
    GIVEN=`echo "$2" | tr "\n" " "`
    if [ "$EXPECTED" == "$GIVEN" ]; then
        GLOBAL_SUMMARY+=("$TEST_NAME : OK")
        return $PASS
    else
        GLOBAL_RESULT=$FAIL
        GLOBAL_SUMMARY+=("$TEST_NAME : FAIL")
        GLOBAL_SUMMARY+=("    expected: \"$EXPECTED\" not equal \"$GIVEN\"")
        return $FAIL
    fi
}

function run_tests()
{
    TESTS=`declare -F|grep -o "\<test\S\+"`
    for item in ${TESTS[*]}
    do
        ${item}
    done

    printf '%s\n' "${GLOBAL_SUMMARY[@]}"
    return $GLOBAL_RESULT
}

#WIP
function find_test_suits()
{
    SUITES=`find . -iname "*test*"`
    echo $SUITES
}

#WIP
function run_test_suites()
{
    SUITES_=`find_test_suits`
    . ./$SUITES_
    run_tests
}

