#!/bin/bash
TEST_FILE_PATERN="test*"
TEST_SUITES=`find . -iname "$TEST_FILE_PATERN"`

PASS=0
FAIL=1
RESULT=$PASS

for SUITE in ${TEST_SUITES[*]}
do
    echo "Running: $SUITE"
    $SUITE
    if [ $? != $PASS ]; then
        RESULT=$FAIL
    fi
done

exit $RESULT
