#!/bin/bash

OPTIONS=(
"-t"  # open test
"-c"  # open testcase
"-n"  # create new file
)

function displayHelp()
{
    echo "Script use variable \$EDITOR to open files, if it is not set vim is used.

g                   open \$EDITOR
g file_path         open file
g file_name         search for file in current directory and open it
g -n file_name      create new file with given name
g -t test_name      search for TEST_F/TEST_P with given name in curent directory and open it
g -c testcase_name  search for testcase with given name in current directory and open it

to open file on given line with vim/gvim:
g file +line_number
g file:line_number
g file#line_number"
}

function openTestcase()
{
    if [ $# -gt 0 ]; then
        local TEST_NAME="$1"
        local OMITED="\.svn|\.swp|\.tmp"
        local FILE_WITH_LINE="^\S*:[0-9]*"
        local DIR=`grep -rn "testcase\s*$TEST_NAME\s*(" $PWD | egrep -v $OMITED | grep -o $FILE_WITH_LINE`
        echo $DIR
    fi
}

function openTest()
{
    if [ $# -gt 0 ]; then
        local TEST_NAME="$1"
        local TEST_PATERN="TEST\(\|_P\|_F\)" # TEST or TEST_F or TEST_P
        local OMITED="\.svn|\.swp|\.tmp"
        local FILE_WITH_LINE="^\S*:[0-9]*"
        local DIR=`grep -rn "$TEST_PATERN\s*(.*,\s*$TEST_NAME\s*)" $PWD | egrep -v $OMITED | grep -o $FILE_WITH_LINE`
        echo $DIR
    fi
}

#remove directory form "./dir/file" -> file
function removeDirectory()
{
    echo $1 | sed -r "s/.*\/([^\/]*)$/\1/"
}

#LNI - Line Number Indicator pattern
LNI='\+[0-9]+'

#change #num or :num to +num
function normalizeLineNumberIndicator()
{
    echo $@ | sed -r "s/[\:#]+([0-9]+)/\+\1/g"
}

#change +num[TRASHES] to +num
function removeTrashesAfterLineNumberIndicator()
{
    echo $@ | sed -r "s/($LNI)[^0-9].*$/\1/"
}

#change "file +num" to "file+num"
function mergeFileWithLineNumberIndicator()
{
    echo $@ | sed -r "s/(\S)\s+($LNI)/\1\2/g"
}

#change file+num to file
function removeLineNumberIndicator()
{
    echo $@ | sed -r "s/$LNI//g"
}

#check if given arg is existing file
function isFile()
{
    [ -e $@ ]
}

#serch for file with find
function findFile()
{
    find . -name $@ | egrep -v "\.svn|\.swp|\.tmp"
}

#return filename "some_dir/filename+num" -> "file_name"
function getFileName()
{
   removeDirectory `removeLineNumberIndicator $@`
}

#if file is feile directory return file else serch for file
function getFileDirectory()
{
    local fileDir=$(removeLineNumberIndicator $@)
    if isFile $fileDir ; then
        echo $fileDir
    else
        findFile `getFileName $fileDir`
    fi
}

#extract LNI file+num -> +num
function getLineNumberIndicator()
{
    echo $@ | grep -Eo "$LNI"
}

#serch for given files and append LNI if ile is found
function getFiles()
{
    for file in $@; do
        local file_dir=$(getFileDirectory $file)
        local lni=$(getLineNumberIndicator $file)
        if [ ${#file_dir} -gt 0 ] ; then
            echo $file_dir $lni
        fi
    done;
}

#MAIN
[ -z "$EDITOR" ] && EDITOR=vim #if EDITOR is not set use vim
[ $# -eq 0 ] && $EDITOR && exit #open EDITOR if no arguments privuded

INPUT=$@;

#optoins handling
while getopts "hn:c:t:" opt; do
  case $opt in
    h) displayHelp && exit ;;            # help
    t) INPUT=$(openTest $OPTARG) ;;      # open Test
    c) INPUT=$(openTestcase $OPTARG) ;;  # open testcase
    n) $EDITOR $OPTARG && exit ;;        # new File
    *) exit 1 ;;
  esac
done

INPUT=$(normalizeLineNumberIndicator $INPUT)
INPUT=$(mergeFileWithLineNumberIndicator $INPUT)
INPUT=$(removeTrashesAfterLineNumberIndicator $INPUT)
FILES=$(getFiles $INPUT)
#echo $FILES
if [ ${#FILES} -gt 0 ]; then #any file to open?
    $EDITOR $FILES
else
    echo "No files to open"
fi

